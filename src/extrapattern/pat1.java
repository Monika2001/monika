/* Write a Java program to print the following grid. Go to the editor
Expected Output :
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - -
- - - - - - - - - - */
package extrapattern;

public class pat1 {
    public static void main(String[] args) {
        int line=10;
        int star=10;
        for (int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                System.out.print("-"+ " ");
            }
            System.out.println();
        }
    }
}
