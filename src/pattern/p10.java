/*
1 2 3 4 5
2 3 4 5 6
3 4 5 6 7
4 5 6 7 8
5 6 7 8 9
 */
package pattern;

public class p10  {
    public static void main(String[] args){
        int line=5;
        int star=5;
        int ch=1;
        for (int i=0;i<line;i++){
            int ch1=ch;
            for (int j=0;j<star;j++){
                System.out.print(ch1++ + " ");

            }
            System.out.println();
            ch+=1;

                }
    }

}

