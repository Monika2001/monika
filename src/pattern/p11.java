/*
A B C D E
B C D E F
C D E F G
D E F G H
E F G H I

 */
package pattern;

public class p11 {
    public static void main(String[] args){
        int line=5;
        int star=5;
        char ch='A';
        for (int i=0;i<line;i++){
            char ch1=ch;
            for (int j=0;j<star;j++){
                System.out.print(ch1++ + " ");

            }
            System.out.println();
            ch+=1;

        }
    }


}
