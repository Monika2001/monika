/*
1 2 3 4 5
A B C D E
1 2 3 4 5
A B C D E
1 2 3 4 5
 */
package pattern;
public class p13 {
    public static void main(String[] args) {
    int line = 5;
    int star = 5;
    for (int i = 0; i < line; i++) {
        int ch = 1;
        char ch1= 'A';
        for (int j = 0; j < star; j++) {
            if (i % 2 == 0) {
                if (ch <= 5)
                    System.out.print(ch++ +" ");
            }
            else if(ch1<='E') {
            System.out.print(ch1++ + " ");
        }
    }
        System.out.println();
    }
    }
}

