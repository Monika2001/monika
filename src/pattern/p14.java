/*
0 1 0 1 0
0 1 0 1 0
0 1 0 1 0
0 1 0 1 0
0 1 0 1 0
 */
package pattern;
public class p14 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if (j % 2 != 0) {
                    System.out.print(ch + " ");
                } else {
                    System.out.print(0 + " ");
                }
            }
            System.out.println();
        }
    }
}
