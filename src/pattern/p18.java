
/*
    5
   45
  345
 2345
12345
*/
package pattern;

public class p18 {
    public static void main(String[] args) {
        int row=5;
        int col=1;
        int space=row-1;
        int ch=5;
        for (int i=0;i<row;i++){
            int ch1=ch;
            for (int k =0 ; k < space ; k++)
                System.out.print(" ");

            for (int j =0 ; j< col ; j++)
                System.out.print(ch1++ );

            ch--;
            System.out.println();
            col++;
            space--;
        }
    }
}
