/*
    5
   56
  567
 5678
56789
 */
package pattern;
public class p19 {
    public static void main(String[] args) {
        int row =5;
        int col=1;
        int space=row-1;
        int ch=5;
        for (int i=0;i<row;i++){
            int ch1=ch;
            for (int k=0;k<space;k++) {
                System.out.print(" ");
            }

                for (int j=0;j<col;j++) {
                    System.out.print(ch1++);
                }
                System.out.println();
            space--;
            col++;
        }
        ch--;
    }
}
