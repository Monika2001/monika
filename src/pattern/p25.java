/*
11111
*****
33333
*****
55555
 */
package pattern;
public class p25 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch = 1;
        for (int i = 0; i < line; i++) {

            for (int j = 0; j < star; j++) {
                if (i % 2 == 0) {
                    if(ch<=5)
                        System.out.print(ch);

                } else {
                    System.out.print("*");

                }

            }
            ch++;

            System.out.println();

        }
    }
}
