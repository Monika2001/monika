package pattern;

public class p30 {
    public static void main(String[] args) {
        int line = 7;
        int star = 4;
        int space = line - 1;
        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.println(" ");
                for (int j = 0; j < star; j++) {

                    System.out.print("*" +" ");
                    System.out.println();
                    if (i <= 3) {
                        star--;
                        
                    } else {
                        star++;
                    }

                }

            }
        }
    }
}
