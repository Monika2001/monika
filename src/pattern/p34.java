/*
        * * * * * * * * *
          * * * * * * *
            * * * * *
              * * *
                *
              * * *
            * * * * *
          * * * * * * *
        * * * * * * * * *

*/
package pattern;
public class p34 {
    public static void main(String[] args) {
        int line = 9;
        int star = 9;
        int space = line - 1;
        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++)
                System.out.print(" ");

            for (int j = 0; j < star; j++)
                System.out.print("*"+" ");
            System.out.println();

            if (i <= 3) {
                star-=2;
                space+=2;
            } else {
                star+=2;
                space-=2;
            }
        }
    }
}






