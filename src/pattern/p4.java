/*
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
 */
package pattern;

public class p4 {
    public static void main(String[] args){
        int line=5;
        int star=5;

        for(int i=0;i<line;i++){
            int ch=1;
            for(int j=0;j<star;j++){
                System.out.print( ch );
                ch++;
                System.out.print(" ");
            }
            System.out.println( );
        }
    }
}
