/*33)
F
FE
FED
FEDC
FEDCB
FEDCBA
 */
package pattern;

public class p47 {
    public static void main(String[] args) {
        int line = 6;
        int star = 1;
        for (int i = 0; i < line; i++) {
            char ch = 'F';
            for (int j = 0; j < star; j++)
                System.out.print(ch--);

            System.out.println();
            star++;
        }
    }
}
