package pattern;

public class p51 {
    public static void main(String[] args) {
        int line=9;
        int star1=5;
        int star2=5;
        int space=0;
        for (int i=0;i<line;i++){
            for (int j=0;j<star1;j++)
                System.out.print("*");
            for (int k=0;k<space;k++)
                System.out.print(" ");
            for (int j=0;j<star2;j++)
                System.out.print("*");
            System.out.println();
            if(i<4){
                star1--;
                star2--;
                space+=2;
            }
            else {
                star1++;
                star2++;
                space-=2;
            }
        }
    }
}
