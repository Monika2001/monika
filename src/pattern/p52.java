//42]

package pattern;

public class p52 {
    public static void main(String[] args) {
        int line = 10;
        int star = 5;
        int ch=1;
        int space=0;
        for (int i = 0; i < line; i++) {
            int ch1=ch;
            for (int k=0;k<space;k++)
                System.out.print(" ");
            for (int j = 0; j < star; j++)
                System.out.print(ch1++ +" ");
            System.out.println();
            if (i < 4) {
                star--;
                space++;
                ch++;
            }
            else if(i>4){
                star++;
                space--;
                ch--;
            }
        }
    }
}
