package pattern;

public class p53 {
    public static void main(String[] args) {
        int line = 7;
        int star = 10;
        for (int i = 0; i < line; i++) {
            int ch = 0;
            for (int j = 0; j < star; j++) {
                for (int k = 0; k < 5; k++)
                    System.out.print(ch);
            ch++;
        }
            System.out.println();
    }

    }
}
