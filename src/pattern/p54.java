/*
N       N
 N     N
  N   N
   N N
    N
 */
package pattern;
public class p54 {
    public static void main(String[] args) {
        int line = 5;
        int star = 9;
        int space = 0;
        for (int i = 0; i < line; i++) {

            for (int k = 0; k < space; k++)
                System.out.print(" ");
            for (int j = 0; j < star; j++)
                if (j == 0 || j == star - 1)
                    System.out.print("N");
                else
                    System.out.print(" ");

            System.out.println();
            star -= 2;
            space++;
        }
    }
}
