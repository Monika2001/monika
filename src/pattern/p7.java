/*
1 2 3 4 5
6 7 1 2 3
4 5 6 7 1
2 3 4 5 6
 */
package pattern;

public class p7 {
    public static void main(String[] args) {
        int line = 4;
        int star = 5;
        int ch = 1;
        for( int i=0;i<line ;i++)
        {
            for (int j = 0; j < star; j++) {
                System.out.print(ch +" ");
                ch++;
                if (ch > 7) {
                    ch = 1;
                }

            }
            System.out.println();
        }
    }
}
