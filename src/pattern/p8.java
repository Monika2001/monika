/*
A B C D
E A B C
D E A B
C D E A
 */
package pattern;

public class p8 {
    public static void main(String[] args) {
        int line = 4;
        int star = 4;
        char ch = 'A';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print(ch++ +" ");


                if (ch >'E') {
                    ch = 'A';
                }
            }
            System.out.println();
        }
    }
}
