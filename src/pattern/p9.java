/*
1 6 11 16 21
2 7 12 17 22
3 8 13 18 23
4 9 14 19 24
5 10 15 20 25
 */
package pattern;

public class p9 {
    public static void main(String[] args){
        int line=5;
        int star=5;
        int ch=1;
        for (int i=0;i<line;i++){
            int ch1=ch;
            for (int j=0;j<star;j++){
                System.out.print(ch1++ + " ");
                ch1+=4;
            }
            System.out.println();
            ch++;
        }
    }

}
