package stringclass;

public class StringDemo {
    public static void main(String[] args){
        String s = "Software Developer";
        System.out.println(s.length());
        System.out.println(s.charAt(2));
        System.out.println(s.indexOf('e'));
        System.out.println(s.lastIndexOf('e'));
        System.out.println(s.contains("eve"));
        System.out.println(s.startsWith("Soft"));
    }
}
